package com.example.prueba.model;

import com.example.prueba.util.EstadoCivilEnum;

public class Empleado extends Persona{
	
	private int codigoEmpleado;
	private String cargo;
	private int salario;


	public Empleado(int codigoEmpleado, String nombres, String apellidos, String telefono, String direccion,
			EstadoCivilEnum estadoCivil, String cargo, int salario) {
		super(nombres, apellidos, telefono, direccion, estadoCivil);
		this.codigoEmpleado = codigoEmpleado;
		this.cargo = cargo;
		this.salario = salario;
	}

	public int getCodigoEmpleado() {
		return codigoEmpleado;
	}

	public void setCodigoEmpleado(int codigoEmpleado) {
		this.codigoEmpleado = codigoEmpleado;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}
	
}
