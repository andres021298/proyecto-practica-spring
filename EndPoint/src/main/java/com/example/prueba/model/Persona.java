package com.example.prueba.model;

import com.example.prueba.util.EstadoCivilEnum;

public class Persona {

	private String nombres;
	private String apellidos;
	private String telefono;
	private String direccion;
	private EstadoCivilEnum estadoCivil;
	
	
	
	public Persona(String nombres, String apellidos, String telefono, String direccion, EstadoCivilEnum estadoCivil) {
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.direccion = direccion;
		this.estadoCivil = estadoCivil;
	}
	
	public Persona() {
		this.estadoCivil = EstadoCivilEnum.SOLTERO;
	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public EstadoCivilEnum getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	
}
