package com.example.prueba.util;

public enum EstadoCivilEnum {
	
	SOLTERO,
	CASADO,
	SEPARADO,
	VIUDO
	
}
