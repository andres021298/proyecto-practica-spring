package com.example.prueba.service;

import java.util.List;
import java.util.concurrent.Future;

import org.springframework.web.multipart.MultipartFile;

import com.example.prueba.model.Empleado;

public interface EmpleadoService {

	public void agregarEmpleado(Empleado nuevoEmpleado);
	
	public List<Empleado> listarEmpleados();
	
	public Empleado buscarEmpleado(int codigo);
	
	public String eliminarEmpleado(int codigo);
	
	public int sumarSalarios();
	
	public Empleado seleccionarEmpleadoAleatorio();
	
	public Future<String> trabajar(Empleado emp) throws InterruptedException;
	
	public void exportarNomina();
	
	public int subidaMasivaEmpleados(MultipartFile archivo);
}
