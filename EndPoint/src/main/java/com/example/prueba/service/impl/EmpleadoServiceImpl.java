package com.example.prueba.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.prueba.model.Empleado;
import com.example.prueba.service.EmpleadoService;
import com.example.prueba.util.EstadoCivilEnum;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	List<Empleado> empleados = new ArrayList<>();

	@Override
	public void agregarEmpleado(Empleado nuevoEmpleado) {
		// TODO Auto-generated method stub
		this.empleados.add(nuevoEmpleado);
	}

	@Override
	public List<Empleado> listarEmpleados() {
		// TODO Auto-generated method stub
		return this.empleados;
	}

	@Override
	public Empleado buscarEmpleado(int codigo) {
		// TODO Auto-generated method stub
		Empleado empleadoEncontrado = null;
		for (Empleado empleado : empleados) {
			if (empleado.getCodigoEmpleado() == codigo) {
				empleadoEncontrado = empleado;
				break;
			}
		}
		return empleadoEncontrado;
	}

	@Override
	public String eliminarEmpleado(int codigo) {
		// TODO Auto-generated method stub
		String nombreEmpleado = null;
		for (Empleado empleado : empleados) {
			if (empleado.getCodigoEmpleado() == codigo) {
				nombreEmpleado = empleado.getNombres() + " " + empleado.getApellidos();
				this.empleados.remove(empleado);
				break;
			}
		}
		return nombreEmpleado;
	}

	@Override
	public int sumarSalarios() {
		// TODO Auto-generated method stub
		int nomina = 0;
		for (Empleado empleado : empleados) {
			nomina += empleado.getSalario();
		}
		return nomina;
	}

	@Override
	public Empleado seleccionarEmpleadoAleatorio() {
		// TODO Auto-generated method stub
		if (this.empleados.isEmpty())
			return null;
		else {
			int numAleatorio = new Random().nextInt(this.empleados.size());
			System.out.println("El numero aleatorio es: " + numAleatorio);
			Empleado empleado = this.empleados.get(numAleatorio);
			return empleado;
		}
	}

	@Override
	@Async
	public Future<String> trabajar(Empleado emp) throws InterruptedException {
		// TODO Auto-generated method stub
		System.out.println(emp.getNombres() + " " + emp.getApellidos() + "comenzó a trabajar");
		long start = System.currentTimeMillis();
		Thread.sleep(new Random().nextInt(15000));
		long end = System.currentTimeMillis();
		return new AsyncResult<String>(emp.getNombres() + " " + emp.getApellidos() + " duró trabajando "
				+ ((end - start) / 1000) + " segundos");
	}

	@Override
	public void exportarNomina() {
		// TODO Auto-generated method stub

		// Creamos un nuevo libro de trabajo
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Usamos la librería .ss para trabajar hojas excel
		Sheet sheet = workbook.createSheet("Nómina TopGroup");

		// Configuramos el estilo que vamos a crear
		XSSFCellStyle cellStyle = workbook.createCellStyle();

		// Tipo de relleno
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		// Color del fondo
		cellStyle.setFillBackgroundColor(IndexedColors.SKY_BLUE.getIndex());

		// TIpo de alineacion
		cellStyle.setAlignment(HorizontalAlignment.CENTER);

		// Color de la letra y agregarle la configuracion de la letra al estilo
		Font font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		cellStyle.setFont(font);

		// Encabezado
		Row row = sheet.createRow(0);
		Cell cellName = row.createCell(0);
		Cell cellSalario = row.createCell(1);
		cellName.setCellValue("Nombre");
		cellSalario.setCellValue("Salario");
		cellName.setCellStyle(cellStyle);
		cellSalario.setCellStyle(cellStyle);

		int cont = 1;
		// Creamos las filas para cada empleado de la empresa
		for (int index = 0; index < empleados.size(); index++) {
			Row rowEmp = sheet.createRow(cont++);
			String nombre = empleados.get(index).getNombres() + " " + (empleados.get(index).getApellidos());
			rowEmp.createCell(0).setCellValue(nombre);
			int salarioEmp = empleados.get(index).getSalario();
			rowEmp.createCell(1).setCellValue(salarioEmp);
		}

		// Al final muestra el total de los salarios
		Row rowTotal = sheet.createRow(cont);
		rowTotal.createCell(0).setCellValue("Total de nómina:");
		rowTotal.createCell(1).setCellValue(sumarSalarios());

		try {
			// Nombre del archivo
			FileOutputStream out = new FileOutputStream("nómina.xlsx");
			workbook.write(out);
			out.close();
			workbook.close();
			System.out.println("Nómina exportada exitosamente.");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Hubo un fallo al exportar la nómina");
		}

	}

	@Override
	public int subidaMasivaEmpleados(MultipartFile archivo) {
		// TODO Auto-generated method stub
		int cantidadNuevosEmpleados = 0;
		try {
			Workbook workbook = WorkbookFactory.create(archivo.getInputStream());
			Sheet sheet = workbook.getSheetAt(0);
			for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
				Row row = sheet.getRow(rowNum);
				float codigo = Float.parseFloat(row.getCell(0).toString());
				int codigoEmpleado = Math.round(codigo);
				String nombres = row.getCell(1).toString();
				String apellidos = row.getCell(2).toString();
				row.getCell(3).setCellType(CellType.STRING);
				String telefono = row.getCell(3).toString();
				String direccion = row.getCell(4).toString();
				String cargo = row.getCell(6).toString();
				int salario = Math.round(Float.parseFloat(row.getCell(7).toString()));
				
				Empleado nuevoEmpleado = new Empleado(codigoEmpleado, nombres, apellidos, telefono, direccion, EstadoCivilEnum.SEPARADO, cargo, salario);
			
				this.agregarEmpleado(nuevoEmpleado);
				cantidadNuevosEmpleados++;
			}
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cantidadNuevosEmpleados;
	}
}
