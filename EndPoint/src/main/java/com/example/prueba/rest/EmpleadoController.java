package com.example.prueba.rest;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.prueba.model.Empleado;
import com.example.prueba.service.EmpleadoService;

@EnableScheduling
@RestController
@RequestMapping(value = "empleados")
public class EmpleadoController {

	@Autowired
	EmpleadoService empleadoService;

	@GetMapping(value = "saludo")
	public ResponseEntity<String> saludar() {
		String miSaludo = "Hola, estas en TopGroup!!";
		return ResponseEntity.ok(miSaludo);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Empleado>> verEmpleados() {
		List<Empleado> empleados = empleadoService.listarEmpleados();
		if (empleados.isEmpty()) {
			System.out.println("La empresa sin empleados");
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(empleados);
	}

	@PostMapping(value = "nuevo")
	public ResponseEntity<String> agregarEmpleado(@RequestBody Empleado nuevoEmpleado) {
		empleadoService.agregarEmpleado(nuevoEmpleado);
		String mensaje = nuevoEmpleado.getNombres() + " " + nuevoEmpleado.getApellidos()
				+ " vinculado correctamente TopGroup";
		return ResponseEntity.ok(mensaje);

	}

	@DeleteMapping(value = "despedir/{codigo}")
	public ResponseEntity<String> despedirEmpleado(@PathVariable("codigo") int codigo) {
		String nombreEmplado = empleadoService.eliminarEmpleado(codigo);
		if (nombreEmplado == null) {
			System.out.println("Empleado no trabaja en la empresa actualmente");
			return ResponseEntity.notFound().build();
		} else {
			System.out.println(nombreEmplado + " fue despedido correctamente");
			return ResponseEntity.ok(nombreEmplado + " fue despedido correctamente");
		}

	}

	@GetMapping(value = "buscar/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Empleado> buscarEmpleado(@PathVariable("codigo") int codigo) {
		Empleado empleado = empleadoService.buscarEmpleado(codigo);
		if (empleado == null) {
			System.out.println("Empleado no contratado por el momento");
			return ResponseEntity.notFound().build();
		} else {
			System.out.println("Nombre empleado: " + empleado.getNombres() + " " + empleado.getApellidos());
			System.out.println("Cargo: " + empleado.getCargo());
			return ResponseEntity.ok(empleado);
		}
	}

	@GetMapping(value = "nomina")
	public ResponseEntity<String> calcularNomina() {
		int nomina = empleadoService.sumarSalarios();
		return ResponseEntity.ok("La nomina de la empresa es: " + nomina);
	}
	
	@Scheduled(cron="0 * * * * ?")
	public void generarNominaProgramada() {
		int nomina = empleadoService.sumarSalarios();
		System.out.println("La nomina de la empresa es: " + nomina);
	}
	
	@Scheduled(fixedRate = 20000)
	public void trabajar() throws InterruptedException, ExecutionException {
		Empleado empleado = this.empleadoService.seleccionarEmpleadoAleatorio();
		Empleado empleado2 = this.empleadoService.seleccionarEmpleadoAleatorio();
		if (empleado == null || empleado2 == null) {
			System.out.println("No hay emplados en la empresa");
		} else {
			Future<String> res = this.empleadoService.trabajar(empleado);
			Future<String> res2 = this.empleadoService.trabajar(empleado2);
			System.out.println(res.get());
			System.out.println(res2.get());
		}
	}
	
	@GetMapping("exportarNomina")
	public ResponseEntity<String> exportarNonima(){
		this.empleadoService.exportarNomina();
		return ResponseEntity.ok("Nomina generada");
	}
	
	@GetMapping("subidaMasiva")
	public ResponseEntity<String> subidaMasivaEmpleados(@RequestParam("nomina") MultipartFile archivo){
		int numEmpleados = this.empleadoService.subidaMasivaEmpleados(archivo);
		return ResponseEntity.ok(numEmpleados + " empleados nuevos agregados correctamente");
	}
	
	
    // Se ejecuta cada 5 segundos con un retraso de 10 segundos al inicio
//    @Scheduled(fixedRate = 5000, initialDelay = 10000)
//    public void tarea1() {
//        System.out.println("Tarea programada cada 5 segundos");
//    }
	
//	 @Scheduled(cron="0/10 * * 27 9 ?")
//	 public void tareConCron() {
//		 System.out.println("tarea con cron ejecutandose");
//	 }
}
